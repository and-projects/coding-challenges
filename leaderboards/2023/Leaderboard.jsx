import React from 'react';

import data from './Leaderboard.data.json';

export const Leaderboard = () => {
  const sortLeaderboard = (userA, userB) => {
    // rank by challenges completed first
    if (userA.challenges > userB.challenges) return -1;
    if (userA.challenges < userB.challenges) return 1;

    // rank by bonuses completed
    if (userA.bonuses > userB.bonuses) return -1;
    if (userA.bonuses < userB.bonuses) return 1;

    // rank by challenges attempted
    if (userA.attempts > userB.attempts) return -1;
    if (userA.attempts < userB.attempts) return 1;

    // finally rank by name
    return userA.name.localeCompare(userB.name);
  };

  const numeric = { textAlign: 'right' };

  const leaderboardData = data.sort(sortLeaderboard).map((user, i) => ({ ...user, position: `#${i + 1}` }));
  return (
    <div>
      <table style={{ textAlign: 'left' }}>
        <thead>
          <tr>
            <th>Position</th>
            <th style={{ minWidth: '12rem' }}>Name</th>
            <th>Solved</th>
            <th>Bonuses</th>
            <th>Attempted</th>
            <th>Most Elegant</th>
            <th>Most Creative</th>
          </tr>
        </thead>
        <tbody>
          {leaderboardData.map(({ position, name, challenges, bonuses, attempts, elegant, creative }) => (
            <tr key={position}>
              <td style={{ fontWeight: 700 }}>{position}</td>
              <td>{name}</td>
              <td style={numeric}>{challenges}</td>
              <td style={numeric}>{bonuses}</td>
              <td style={numeric}>{attempts}</td>
              <td style={numeric}>{elegant}</td>
              <td style={numeric}>{creative}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
