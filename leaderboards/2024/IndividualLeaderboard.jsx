import React from 'react';

import data from './leaderboard.data.json';

export const IndividualLeaderboard = () => {
  const sortLeaderboard = (userA, userB) => {
    // rank by best solutions first
    if (userA.best > userB.best) return -1;
    if (userA.best < userB.best) return 1;

    // rank by solved challenges second
    if (userA.solved > userB.solved) return -1;
    if (userA.solved < userB.solved) return 1;


    // rank by challenges attempted
    if (userA.attempted > userB.attempted) return -1;
    if (userA.attempted < userB.attempted) return 1;

    // finally rank by name
    return userA.name.localeCompare(userB.name);
  };

  const numeric = { textAlign: 'right' };

  const leaderboardData = data.sort(sortLeaderboard).map((user, i) => ({ ...user, position: `#${i + 1}` }));
  return (
    <div>
      <h2>Individual Leaderboard</h2>
      <table style={{ textAlign: 'left' }}>
        <thead>
          <tr>
            <th>Position</th>
            <th style={{ minWidth: '12rem' }}>Name</th>
            <th>Squad</th>
            <th>Attempted</th>
            <th>Solved</th>
            <th>Best code</th>
          </tr>
        </thead>
        <tbody>
          {leaderboardData.map(({ position, name, squad, solved, attempted, best }) => (
            <tr key={position}>
              <td style={{ fontWeight: 700 }}>{position}</td>
              <td>{name}</td>
              <td>{squad}</td>
              <td style={numeric}>{attempted}</td>
              <td style={numeric}>{solved}</td>
              <td style={numeric}>{best}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
