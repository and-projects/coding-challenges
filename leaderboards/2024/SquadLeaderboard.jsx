import React from 'react';

import squadData from './squad.data.json';

export const SquadLeaderboard = () => {
  const sortLeaderboard = (squadA, squadB) => {
    // rank by best solutions first
    if (squadA.best > squadB.best) return -1;
    if (squadA.best < squadB.best) return 1;

    // rank by challenges solved second
    if (squadA.solved > squadB.solved) return -1;
    if (squadA.solved < squadB.solved) return 1;

    // rank by challenges attempted
    if (squadA.attempted > squadB.attempted) return -1;
    if (squadA.attempted < squadB.attempted) return 1;

    // finally rank by name
    return squadA.name.localeCompare(squadB.name);
  };

  const numeric = { textAlign: 'right' };

  const leaderboardData = squadData.sort(sortLeaderboard).map((squad, i) => ({ ...squad, position: `#${i + 1}` }));
  return (
    <div>
      <h2>Squad Leaderboard</h2>
      <table style={{ textAlign: 'left' }}>
        <thead>
          <tr>
            <th>Position</th>
            <th style={{ minWidth: '12rem' }}>Squad Name</th>
            <th>Attempted</th>
            <th>Solved</th>
            <th>Best code</th>
          </tr>
        </thead>
        <tbody>
          {leaderboardData.map(({ position, name, solved, attempted, best }) => (
            <tr key={position}>
              <td style={{ fontWeight: 700 }}>{position}</td>
              <td>{name}</td>
              <td style={numeric}>{attempted}</td>
              <td style={numeric}>{solved}</td>
              <td style={numeric}>{best}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
