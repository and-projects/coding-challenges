---
title: Introduction
---

# Welcome to Coding Challenges!

New coding challenges will be posted and everyone have a chance to attempt to solve it. There will be up to one winner each challenge voted by the community as the best solution.

There are no hard rules and is intended for a bit of fun learning. You can use any language you prefer and feel free to work with others to solve the challenges.

Join us on slack at: [#coding-challenges](https://and-almeida.slack.com/archives/C05FDJD8T0V)

Happy coding!
