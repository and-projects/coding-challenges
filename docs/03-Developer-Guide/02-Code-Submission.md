# Code Submission

The code submission form can be rendered by simply using the `SubmitSolution` component:

```jsx
<SubmitSolution challengeNumber={1} showBonusField />
```

When coding solutions are submitted, they make an API call to the backend which uses gitlab's API to commit and upload a markdown file onto a branch named `challenge-{n}`. The only reason there is a backend is to hide the access token from the network tab when calling gitlab as there is no way to hide this if making the call directly from the frontend.

Ideally, we would want a project access token which only allows updates to the frontend repository, but as it is a paid feature, a [coding_challenges gitlab account](https://gitlab.com/coding-challenges-051) was created which only has access to the coding challenges repositories and the account access token of this account is used instead as a workaround.

Once we want to close the coding challenge, we can simply merge the branch into `main` and use the component: `src/components/CodeAttempts.jsx` with the challenge number to render all attempts.

```jsx
import {CodeAttempts} from '../src/components/CodeAttempts';

## All Attempts
<details>
  <summary>Show all attempts</summary>
  <CodeAttempts challengeNumber={1}/>
</details>
```

:::caution

The code submission system only works if the branch already exists.
:::
