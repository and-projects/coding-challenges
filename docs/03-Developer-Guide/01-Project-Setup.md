# Project Setup

## Docusaurus

The website is created using the [docusaurus](https://docusaurus.io/) library which is designed to create a document website using markdown files. I have found it to be super powerful and flexible as it is compatible with `.mdx` files which are essentially markdown files on steroids and allow you to inject React components directly into the markdown file:

```
export const Highlight = ({children, color}) => (
  <span
    style={{
      backgroundColor: color,
      borderRadius: '2px',
      color: '#fff',
      padding: '0.2rem',
    }}>
    {children}
  </span>
);

<Highlight color="#25c2a0">Docusaurus green</Highlight> and <Highlight color="#1877F2">Facebook blue</Highlight> are my favorite colors.

I can write **Markdown** alongside my _JSX_!
```

## Code Base

There is both a frontend and a backend and both can be started by simply installing the packages and running the start script:

- Frontend code: https://gitlab.com/and-projects/coding-challenges
- Backend code: https://gitlab.com/and-projects/coding-challenges-be

```
npm i
npm start
```

### Frontend

The coding challenges website is deployed using [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) and the deployment script is outlined in the file `.gitlab-ci.yml`. It runs whenever a merge or push to `main` occurs.

### Backend

The backend requires a `.env` file with an `ACCESS_TOKEN` for making gitlab API calls and a `PROJECT_ID` which is `47320051` and can be found on the gitlab project page. The access token for the deployed backend is currently using a personal access token from a new account which only has access to the coding challenges repositories. A project access token would have been much better but it is a paid feature, so the new account is a workaround for this.

The backend is currently hosted on https://glitch.com/

And the link to the code can be accessed here (requires being added to the project): https://glitch.com/edit/#!/delightful-repeated-nylon

The reason for using glitch is simply because it's free and has generous limits (up to 4000 requests per hour). To view all the restrictions, see [here](https://glitch.happyfox.com/kb/article/17-technical-restrictions/#:~:text=Apps%20are%20limited%20to%204000,Too%20Many%20Requests%22%20response).
