# Format and Rules

There will be a new coding challenge posted at the start of every month and everyone will have until the last Friday of the month to attempt to solve it. The community will review submissions and vote for the best solution on the Friday. 

There are no limitations on what coding language you can use - you may even want to challenge yourself and try a language you've never used before! It's all friendly competition so it is encouraged you work on the challenges with others, especially for the more difficult challenges.

## Leaderboards

There is both a squad leaderboard and an individual leaderboard. Submissions are recorded and tracked based on the number of challenges solved, attempted, and chosen as best solution. This extends to the squad leaderboards - as long as anyone has submitted a solution from the squad, the squad will be rewarded with the point. Currently, the squad can only gain one point per challenge - this is to prevent point inflation from multiple submissions from people belonging to the same squad.
