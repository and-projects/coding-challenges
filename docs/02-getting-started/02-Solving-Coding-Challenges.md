# Solving Coding Challenges

All the coding challenges are intended to be solvable without having to write a lot of code and should fit within a single file. You will have to solve the challenge in your own integrated development environment (IDE). If it is your first time coding, it is recommended you use an online IDE to get going quickly without any installations required.

## Online IDEs

An IDE is a tool used to write and execute your code. A recommended online IDE is [Programiz](https://www.programiz.com/python-programming/online-compiler/) as there is a sidebar that allows you to choose from many different languages.
