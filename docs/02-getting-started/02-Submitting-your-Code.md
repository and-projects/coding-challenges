# Submitting your Code

Within each coding challenge post there will be a section to submit your code. All fields except for the code are optional - if you do not want your name displayed you should leave it blank to remain anonymous. All submissions will be displayed on the post when the coding challenge has closed for others to vote on the best solution.

Ideally, you should submit your answer once but multiple submissions are allowed - and it is possible for a maintainer to manually remove previous submissions.

:::caution
Once you have made a submission, you cannot edit it again. You may make multiple submissions but the maintainer will likely delete all older submissions. Please contact a maintainer if you wish to amend or delete a submission.
:::
