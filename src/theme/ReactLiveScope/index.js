import React from 'react';
import { Uint1Array } from './Uint1Array';
import { test006, test006bonus } from './palindromeChallenge';

const genPrimes = limit => {
  const sieve = new Uint8Array(limit - 1);
  for (let index = 0; index < sieve.length; index++) {
      const sieveValue = sieve[index];
      const prime = index + 2;
      if (sieveValue) continue;
      for (let cancel = (prime ** 2) - 2; cancel < sieve.length; cancel += prime) {
          sieve[cancel] = 1;
      }
  }
  
  const primes = sieve.reduce((primes, val, i) => {
      if (!val) {
          primes.push(i + 2);
      }
      return primes;
  }, [])
  return primes;
}

const log = fn => {
  try {
    const results = fn();
    return <p>{results}</p>
  } catch (e) {
    console.error(e);
    return null;
  }
}

const ReactLiveScope = {
  React,
  ...React,
  log,
  genPrimes,
  Uint1Array,
  test006,
  test006bonus
};
export default ReactLiveScope;
