export const test006 = (fn) => {
    const testStrings = [
        [
            "A man, a plan, a canal: Panama", true
        ],
        [
            "No 'x' in 'Nixon'", true
        ],
        [
            "Abba Zabba, you're my only friend", false
        ],
        ["abba", true],
        ["saippuakivikauppias", true],
        ["this is not a palindrome", false],
        ["no", false]
    ];

    const result = testStrings.findIndex(([string, expected]) => {
        return fn(string) !== expected;
    });

    if (result >= 0) {
        return `Failed for string: "${testStrings[result][0]}", expected ${testStrings[result][1]}.`;
    }

    return 'All tests passed!';
}

const shuffle = (string) => {
 return string.split('').sort(() => Math.random() - 0.5).join('')
}

export const test006bonus = (fn) => {
    const testStrings = [
        [
            shuffle("amanaplanacanalpanama"), true
        ],
        [
            shuffle("noxinnixon"), true
        ],
        [
            shuffle("abbayourmyonlyfriend"), false
        ],
        [shuffle("madam"), true],
        [shuffle("saippuakivikauppias"), true]
    ];

    const result = testStrings.findIndex(([string, expected]) => {
        return fn(string) !== expected;
    });

    if (result >= 0) {
        return `Failed for string: "${testStrings[result][0]}", expected ${testStrings[result][1]}.`;
    }

    return 'All tests passed!';
}
