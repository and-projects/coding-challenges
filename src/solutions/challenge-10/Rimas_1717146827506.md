Name: Rimas  
Squad: Electron  
Language: Javascript

```js
function solve(pets, n) {
    let dogs = [], cats = [], catsCaught = 0;

    pets.forEach((pet, i) => {
        pet === 'D' ? dogs.push(i) : cats.push(i);
    });

    while (dogs.length && cats.length) {
        if (Math.abs(dogs[0] - cats[0]) <= n) {
            catsCaught++;
            dogs.shift();
            cats.shift();
        } else if (cats[0] < dogs[0]) {
            cats.shift();
        } else {
            dogs.shift();
        }
    }

    return catsCaught;
}
```
