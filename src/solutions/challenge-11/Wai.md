Name: Wai
Squad: Helix  
Language: Javascript

```js
function getBestWord(points,words){
  const calcedWords = words.map((word, index) => {
    const score = [...word].reduce((score, letter) => score + points[letter.charCodeAt(0) - 65], 0);
    return { score, length: word.length, index };
  });
  
  return calcedWords.reduce((best, word) => {
    if (word.score !== best.score) {
      return word.score > best.score ? word : best;
    }
    if (word.length !== best.length) {
      return word.length < best.length ? word : best;
    }
    return best;
  }, calcedWords[0]).index;
}
```
