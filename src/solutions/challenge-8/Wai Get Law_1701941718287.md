Name: Wai Get Law  
Language: Javascript

```js
function prizeCounter(hoops) {
    // convert triple hoops to a single lowercase letter representation
    const bonusHoops = hoops.join('').replace(/R{3}/g, 'r').replace(/G{3}/g, 'g').replace(/B{3}/g, 'b')
    // only one can be deactivated at a time as others get reactivated when a new hoop is deactivated
    let deactivated = '';

    // filter list so that we remove all hoops that are made when deactivated
    const filteredHoops = [...bonusHoops].filter(hoop => {
        // if hoop is not deactivated, keep the hoop
        if (deactivated !== hoop.toLowerCase()) {
            // if hoop is lowercase i.e., 3 in a row, set as new deactivated
            if (hoop === hoop.toLowerCase()) {
                deactivated = hoop;
            }
            return true
        }
        // hoop was deactivated so remove from list
        return false
    })

    // equivalent points for 3 in a row e.g. R R R = 100 + 100 + 100 + 500 = 800
    const points = { r: 800, b: 600, g: 500 };
    return filteredHoops.reduce((total, hoop) => total + (points[hoop] || 100), 0)
}
```
