Name: Jim  
Language: Python

```python
def prizeCounter(hoops):
    score = 0
    consecutive_count = 0
    current_hoop = None
    completed_hoops = []

    for hoop in hoops:
        if current_hoop == hoop:
            consecutive_count += 1
        else:
            consecutive_count = 1
            current_hoop = hoop

        if current_hoop not in completed_hoops:
            if consecutive_count == 3:
                if current_hoop == 'R':
                    score += 600
                elif current_hoop == 'B':
                    score += 400
                elif current_hoop == 'G':
                    score += 300
                completed_hoops.append(current_hoop)
            else:
                score += 100
        
    return score

print("Total score:", prizeCounter(['R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R']))
```
