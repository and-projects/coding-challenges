Name: Jamie  
Answer: 1074  
Bonus: 5.973525387668077e+199  
Language: Javascript

```js
function findMaximumPathSum(triangle) {
  function findBottomUpTotal() {
    const lastLine = triangle.pop();
    const aboveLine = triangle.pop();
    triangle.push(
      aboveLine.map((num, i) =>
        Math.max(num + lastLine[i], num + lastLine[i + 1])
      )
    );
  }

  do {
    findBottomUpTotal();
  } while (triangle.length > 1);
  return triangle[0][0];
}
```
