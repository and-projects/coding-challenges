Name: Wai Get Law  
Score: 600

```
<div>
    <div class="stem"/>
  <div>
    <div class="stem-circle" />
    <div class="stem-circle-left" />
  </div>
  <div class="club">
    <div class="circle top" />
    <div class="circle lower-right" />
    <div class="circle lower-left" />
  </div>
</div>

<style>
  body { background-color: #4F77FF}
  .club {
    position: absolute;
    top: -160px;
    left: -5px
  }
  .circle {
    width: 100px;
    height: 100px;
    border-radius: 100%;
    background: #1038BF;
    position: absolute;
  }
  .stem-circle {
    width: 100px;
    height: 100px;
    border-radius: 100%;
    background: #4F77FF;
    position: absolute;
    left: 205px;
    top: 160px;
  }
   .stem-circle-left {
    width: 100px;
    height: 100px;
    border-radius: 100%;
    background: #4F77FF;
    left: -110px;
     position:absolute;
  }
  .top {
    top: 50px;
    left: calc(50% - 50px);
  }
  .lower-right {
    top: 70px;
    left: 45%
  }
  .lower-left {
    top: 0rem;
    right: 90%
  }
  .stem {
    height: 82px;
    width: 50px;
    margin-left: 165px;
    margin-top: 168px;
    background: #1038BF;
  }
</style>
```
