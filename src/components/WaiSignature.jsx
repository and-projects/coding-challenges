import React, { useState } from 'react';
import { useColorMode } from '@docusaurus/theme-common';
import "./WaiSignature.css";

export const WaiSignature = () => {
    const { colorMode } = useColorMode();
    const [copied, setCopied] = useState(false);
    
    const friendKey = '1091383_d682XZBbhrv34RYhcP90mKq34Dt8wyHn';

    const copyToClipboard = () => {
        navigator.clipboard.writeText(friendKey);
        setCopied(true);
        setTimeout(() => setCopied(false), 1500)
    }
    return (
        <>
        { copied && <div style = {{
             position: 'fixed',
             bottom: '0',
             right: '-4rem',
             /* bring your own prefixes */
             transform: 'translate(-50%, -50%)',
             backgroundColor: 'green',
             padding: '1rem',
             borderRadius: '0.5rem',
             zIndex: 10,
             opacity: 0.8
        }}>Copied to clipboard!</div>}

        <div style={{ display: 'flex', flexDirection: 'column', marginTop: '7rem', borderTop: `1px solid ${colorMode == 'dark' ? 'white' : 'black'}`, padding: '1rem 2rem'}}>
            <h2 className='sig-title'>Wai Get Law</h2>
            <h4 style={{ marginBottom: '1.5rem' }}>Product Developer AND Chess Enthusiast</h4>

            <h4 style={{ marginBottom: '0.5rem' }}>Codewars</h4>
            <div style={{ display: 'flex', marginBottom: '0.5rem' }}>
                <a href="https://www.codewars.com/users/Wai%20Get%20Law" target="_blank">
                    <img src="https://www.codewars.com/users/Wai%20Get%20Law/badges/small" />
                </a>
            </div>

            <h4 style={{ marginBottom: '0.5rem' }}>Project Euler</h4>
            <div style={{ width: '100%', display: 'flex', flexWrap: 'wrap', gap: '0.5rem'}}>
                <a href="https://projecteuler.net/progress=waiget" target="_blank">
                    <img src="https://projecteuler.net/profile/waiget.png" style={{ height: "100%", margin: '0', display: 'block' }} />
                </a>
                <button style={{
                    cursor: 'copy',
                    fontSize: '1rem'
                    }} onClick={copyToClipboard}>
                    copy friend key
                </button>
            </div>
         
            <p style={{ fontStyle: "italic", marginTop: '1rem'}}>"We can not solve our problems with the same level of thinking that created them."<br/>- Albert Einstein</p>
        </div>
        </>
    )
}
