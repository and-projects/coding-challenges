import React from 'react';
import loadable from '@loadable/component';

export const CodeAttempts = ({ challengeNumber }) => {
  const importAll = (r) => r.keys();

  // somewhat hacky to get a list of all files in a folder as it requires a hardcoded path to load them.
  const files = importAll(require.context('../solutions/', true, /\.*md$/));
  const filenames = files
    .filter((f) => f.split('/').includes(`challenge-${challengeNumber}`))
    .map((f) => f.replace('./', ''));
  let attempts = filenames.map((name) => loadable(() => import(`../solutions/${name}`)));

  return (
    <div style={{ padding: '1rem' }}>
      {attempts.map((Attempt, i) => (
        <div key={i}>
          <Attempt />
          {i < attempts.length - 1 ? <hr /> : null}
        </div>
      ))}
    </div>
  );
};
