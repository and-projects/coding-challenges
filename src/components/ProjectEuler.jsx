import React from 'react';
import { useColorMode } from '@docusaurus/theme-common';
export const ProjectEuler = ({ children }) => {
    const { colorMode } = useColorMode();
    return (
        <div style={{ border: `1px solid ${colorMode == 'dark' ? 'white' : 'black'}`, padding: '1rem 1rem 0 1rem', margin: '2rem', borderRadius: '0.5rem' }}>
        {children}
        </div>
    )
}
