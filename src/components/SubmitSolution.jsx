import React, { useState } from 'react';
import axios from 'axios';

export const SubmitSolution = ({
  challengeNumber,
  maxCode = 10000,
  showAnswerField = false,
  showLangSelect = true,
  showBonusField = false,
  showScoreField = false,
  showSquad = true
}) => {
  const [submissionName, setSubmissionName] = useState('');
  const [squad, setSquad] = useState('');
  const [score, setScore] = useState('');
  const [answer, setAnswer] = useState('');
  const [bonus, setBonus] = useState('');
  const [code, setCode] = useState('');
  const [lang, setLang] = useState('');
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const getLangLabel = (val) => langOptions.find((lang) => lang.value === val)?.label || '';
  const langOptions = [
    {
      label: 'Javascript',
      value: 'js',
    },

    {
      label: 'Java',
      value: 'java',
    },

    {
      label: 'C',
      value: 'c',
    },

    {
      label: 'C#',
      value: 'csharp',
    },

    {
      label: 'Go',
      value: 'go',
    },

    {
      label: 'Python',
      value: 'python',
    },
    {
      label: 'R',
      value: 'r',
    },
  ];

  const upload = async () => {
    setError('');
    setLoading(true);
    try {
      const contentMetaData = [
        `Name: ${submissionName || 'anonymous'}`,
        `${squad ? `Squad: ${squad}` : ''}`,
        `${score ? `Score: ${score}` : ''}`,
        `${answer ? `Answer: ${answer}` : ''}`,
        `${bonus ? `Bonus: ${bonus}` : ''}`,
        `${lang ? `Language: ${getLangLabel(lang)}` : ''}`,
      ];
      const content = `${contentMetaData.filter(Boolean).join('  \n')}

\`\`\`${lang}
${code}
\`\`\`
`;
      const filename = `${submissionName || 'anonymous'}_${new Date().getTime()}.md`;
      await axios.post('https://delightful-repeated-nylon.glitch.me/', { challengeNumber, filename, content });
      setSubmitted(true);
    } catch (e) {
      setError('Something went wrong... please try again.');
    } finally {
      setLoading(false);
    }
  };

  console.log(squad)
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
      <h2>Submit Solution</h2>

      <label>Name (leave blank for anonymous):</label>
      <input
        maxLength={50}
        disabled={submitted || loading}
        value={submissionName}
        onChange={(e) => setSubmissionName(e.target.value)}
      />

      {showSquad ? (
        <>
          <label>Squad:</label>
          <select
            disabled={submitted || loading}
            value={squad}
            onChange={(e) => setSquad(e.target.value)}
          >
            <option disabled value="">Please select</option>
            <option>Dalton</option>
            <option>Electron</option>
            <option>Halo</option>
            <option>Hart</option>
            <option>Helix</option>
          </select>
        </>
      ) : null}

      {showScoreField ? (
        <>
          <label>Score:</label>
          <input
            maxLength={50}
            disabled={submitted || loading}
            value={score}
            onChange={(e) => setScore(e.target.value)}
          />
        </>
      ) : null}

      {showAnswerField ? (
        <>
          <label>Answer:</label>
          <input
            maxLength={50}
            disabled={submitted || loading}
            value={answer}
            onChange={(e) => setAnswer(e.target.value)}
          />
        </>
      ) : null}

      {showBonusField ? (
        <>
          <label>Answer (Bonus):</label>
          <input
            maxLength={50}
            disabled={submitted || loading}
            value={bonus}
            onChange={(e) => setBonus(e.target.value)}
          />
        </>
      ) : null}

      {showLangSelect ? (
        <>
          <label>Language:</label>
          <select disabled={submitted || loading} onChange={(e) => setLang(e.target.value)}>
            <option value={''}>Not specified</option>
            {langOptions.map((option) => (
              <option value={option.value} key={option.value}>
                {option.label}
              </option>
            ))}
          </select>
        </>
      ) : null}

      <label>Code:</label>
      <textarea
        disabled={submitted || loading}
        maxLength={maxCode}
        style={{ resize: 'vertical' }}
        rows="12"
        value={code}
        onChange={(e) => setCode(e.target.value)}
      />

      <button
        disabled={!code || submitted || loading}
        style={{
          margin: '1rem auto',
          padding: '0.5rem 3rem',
          width: 'fit-content',
          cursor: loading ? 'wait' : !code || submitted || loading ? 'not-allowed' : 'pointer',
        }}
        onClick={upload}
      >
        Submit
      </button>
      {error ? (
        <div
          style={{
            border: '1px solid #521717',
            backgroundColor: '#FFCCCC',
            borderRadius: '0.5rem',
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
          }}
        >
          <p style={{ margin: 'auto', padding: '0.1rem', color: '#521717' }}>{error}</p>
        </div>
      ) : null}
      {submitted ? (
        <div
          style={{
            border: '1px solid #18410E',
            backgroundColor: '#4BB543',
            borderRadius: '0.5rem',
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
          }}
        >
          <p style={{ margin: 'auto', padding: '0.1rem', color: '#18410E' }}>
            Thank you! Your code has been submitted.
          </p>
        </div>
      ) : null}
    </div>
  );
};
