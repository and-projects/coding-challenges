// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Coding Challenges',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://and-projects.gitlab.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: process.env.IS_LOCAL ? '/' : '/coding-challenges/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  plugins: [
    require.resolve('@cmfcmf/docusaurus-search-local'),
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'second-blog',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: '/blog',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './blog',

        remarkPlugins: [math],
        rehypePlugins: [katex],
      }
    ],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'leaderboards',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: '/leaderboards',
        blogSidebarTitle: 'Leaderboards',
        showReadingTime: false,
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: './leaderboards',

        remarkPlugins: [math],
        rehypePlugins: [katex],
      }
    ]
  ],
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        blog: {
          routeBasePath: '/',
          showReadingTime: false,
          blogSidebarTitle: 'All challenges',
          blogSidebarCount: 'ALL',
          blogTitle: '',
          path: './coding-challenges',
          remarkPlugins: [math],
          rehypePlugins: [katex],
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      image: 'img/social-card.png',
      navbar: {
        title: 'Coding Challenges',
        logo: {
          alt: 'My Site Logo',
          src: 'img/red-coffee-logo.png',
        },
        items: [
          { to: '/leaderboards', label: 'Leaderboards', position: 'left'},
          {
            type: 'docSidebar',
            sidebarId: 'introductionSidebar',
            position: 'left',
            label: 'Docs',
          },
          { to: '/tags', label: 'Tags', position: 'right' },
          {
            type: 'search',
            position: 'right',
          },
          {
            to: '/blog', label: 'Blog', position: 'left'
          }
        ],
      },
      footer: {
        links: [
          {
            title: 'Community',
            items: [
              {
                label: 'Slack',
                to: 'https://and-almeida.slack.com/archives/C05FDJD8T0V',
              },
            ],
          },
          {
            title: 'Technologies',
            items: [
              {
                label: 'Docusaurus',
                to: 'https://docusaurus.io/',
              },
              {
                label: 'Frontend',
                to: 'https://gitlab.com/and-projects/coding-challenges',
              },
              {
                label: 'Backend',
                to: 'https://gitlab.com/and-projects/coding-challenges-be',
              },
            ],
          },
          {
            title: 'Useful Links',
            items: [
              {
                label: 'Programiz (online IDE)',
                to: 'https://www.programiz.com/python-programming/online-compiler/',
              },
              {
                label: 'Stackoverflow',
                to: 'https://stackoverflow.com/',
              },
            ],
          },
          {
            title: 'References',
            items: [
              {
                label: 'Project Euler',
                to: 'https://projecteuler.net/',
              },
              {
                label: 'Codewars',
                to: 'https://www.codewars.com/',
              },
            ],
          },
        ],
        logo: {
          alt: 'ANDigital Almeida Logo',
          src: 'img/ANDigital_almeida_red.jpg',
          href: 'https://sites.google.com/and.digital/almeida-people-portal/our-club-house',
          height: 100,
          target: '_blank',
        },
        copyright: `Coding Challenges • Designed and Built by Club Almeida`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ['java', 'c', 'csharp', 'go', 'python', 'r', 'julia'],
      },
    }),
    stylesheets: [
      {
        href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
        type: 'text/css',
        integrity:
          'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
        crossorigin: 'anonymous',
      },
    ],
    themes: ['@docusaurus/theme-live-codeblock']
};

module.exports = config;
