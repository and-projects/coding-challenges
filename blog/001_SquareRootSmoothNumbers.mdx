---
slug: square-root-smooth-numbers
title: Square Root Smooth Numbers
authors: [wai]
tags: [project-euler, optimisation, prime numbers]
date: 2023-08-19
---

A lot of people I have spoke to seem to be put off by problems that are math themed and I agree they can be scary-looking but when looking - however, most of the problems on Project Euler can be solved by taking the problem and looking at it from a different angle. This 'Eureka' moment always brings me huge enjoyment and worth all the frustration before. In this blog, I will present my journey of trying to solve project euler's problem 668: [Square Root Smooth Numbers](https://projecteuler.net/problem=668).


import { ProjectEuler } from '../src/components/ProjectEuler';

<ProjectEuler>

**Problem 668**

A positive integer is called square root smooth if all of its prime factors are strictly less than its square root.
Including the number $1$, there are $29$ square root smooth numbers not exceeding $100$.

How many square root smooth numbers are there not exceeding $10,000,000,000$?
</ProjectEuler>

<!-- truncate-->

:::caution

Everything from here on are huge spoilers to the problem. If you want to attempt the problem yourself first, look away now. You have been warned!
:::

## Explanation of the Problem

The first step is to understand the problem. Firstly, _"A positive integer"_ means the number must be above zero (zero is neither positive or negative). Next, we need to understand what prime factors are - which requires knowledge of prime numbers.

From [Wikipedia](https://en.wikipedia.org/wiki/Prime_number):

> A prime number (or a prime) is a natural number greater than 1 that is not a product of two smaller natural numbers.

In other words, a prime number is a number that can only be divided wholely by itself and $1$. Note that $1$ itself is not prime, as it specifically says _"a natural number greater than 1"_ and there are a few reasons for this but the main one being due to the [Fundamental theorem of arithmetic](https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic). It states that:

> every integer greater than $1$ can be represented uniquely as a product of prime numbers.

What does this mean? To put it simply, every number greater than $1$ has a unique _"fingerprint"_ made up of prime numbers - where all the prime numbers multiply to the number.

Examples:

$$
\begin{aligned}
15 &= 3\times5\\\
16 &= 2\times2\times2\times2\\\
1200 &= 2\times2\times2\times2\times3\times5\times5\\\
31 &= 31
\end{aligned}
$$

So why does this mean $1$ cannot be prime? The answer is simple. If $1$ was prime, it would break the unique property of this theorem because:

$$
\begin{aligned}
15 &= 3\times5\\\
15 &= 3\times5\times1\\\
15 &= 3\times5\times1\times1
\end{aligned}
$$

So now that we understand this fundamental theorem of arithmetic, we can take a look at some examples to see what is and isn't a square root smooth number.

Taking the earlier examples:

$$
\begin{aligned}
&\text{For 15:}\\\\
&\sqrt{15} \approx 3.9\ \text{and}\ 15 = 3\times5\\\
&\text{since }5 > 3.9\\
&\text{15 is not square root smooth}\\\\

&\text{For 16:}\\\\
&\sqrt{16} = 4\ \text{and}\ 16 = 2\times2\times2\times2\\\
&\text{since }2 < 4\\
&\text{16 is square root smooth}\\\\

&\text{For 1200:}\\\\
&\sqrt{1200} \approx 34.6\ \text{and}\ 1200 = 2^4\times3\times5^2\\\
&\text{since }5 < 34.6\\
&\text{1200 is square root smooth}\\\\

&\text{For 31:}\\\\
&\sqrt{31} \approx 5.6\ \text{and}\ 31 = 31\\\
&\text{since }31 > 5.6\\
&\text{31 is not square root smooth}
\end{aligned}
$$

## Initial Bruteforce Method

The first thought that comes to mind is to calculate all prime factors of a number and then comparing the max prime factor to its square root. The easiest way to calculate its prime factors is to simply try and divide the number by all prime numbers up to and including the square root of the number. We can break early if the number is already reduced down to a $1$:

```js title="calcPrimeFactors.js" showLineNumbers live
log(() => {
    const calcPrimeFactors = (number) => {
        const primes = genPrimes(Math.ceil(Math.sqrt(number)));

        const primeFactors = [];
        let n = number; // copy to not modify original number
        for (const prime of primes) {
            if (n === 1) break;
            while (n % prime === 0) {
                n /= prime;
                primeFactors.push(prime)
            }
        }
        if (n !== 1) primeFactors.push(n);
        return primeFactors;
    }

    const n = 1200;
    return `${n} = ${calcPrimeFactors(n).join(' x ')}`;
})
```
The function `genPrimes` on line 2 will be explained in detail later, but it is simply a function that generates a list of prime numbers from $2$ up to and including the input value. The reason why we only need primes up to the square root of the number is due to the nature of multiplication, the order does not matter.

E.g. $39 = 3\times13 = 13\times3$

Therefore, in the example, when you are checking if $39$ is divisible by $3$, you have simultaneously indirectly checked if the number is divisible by $13$. In general, if you have:

$$
n = a\times b
$$

As $a$ increases, $b$ will decrease, until at some point, $a = b$, and this is what the square root of the number is. We won't have to check for any values of $a > \sqrt{n}$ because we would have already indirectly checked it on our way from $1$ to the square root. This is best seen by example:

$$
\begin{aligned}
&\text{For 36:}\\\
&36 = 1\times36\\\
&36 = 2\times18\\\
&36 = 3\times12\\\
&36 = 4\times9\\\
&36 = 6\times6\\
&\text{(from here we are repeating)}\\\
&36 = 9\times4\\\
&36 = 12\times3\\\
&36 = 18\times2\\\
\end{aligned}
$$

Therefore, if a number, $n$ is divisible by $a$, it is also divisible by $b$ (where $b\ge\sqrt{n}$). Conversely, if $n$ does not divide by any primes up to and including the square root, we also know there cannot be a number greater than the square root that divides $n$ and therefore, we have proved that $n$ must be prime. The full solution of the code checking all $n$ up to a `limit` is now:

```js title="Problem668 v1" showLineNumbers
const genPrimes = require("../JSUtils/genPrimes");

const problem668 = limit => {
    console.time();
    const primes = genPrimes(Math.ceil(Math.sqrt(limit)));
    let totalSquareRootSmoothNumbers = 0;

    const calcPrimeFactors = (number) => {
        const primeFactors = [];
        let n = number; // copy to not modify original number
        for (const prime of primes) {
            if (n === 1) break;
            while (n % prime === 0) {
                n /= prime;
                primeFactors.push(prime)
            }
        }
        if (n !== 1) primeFactors.push(n);
        return primeFactors;
    }

    for (let n = 1; n <= limit; n++) {
        const primeFactors = calcPrimeFactors(n);
        if (Math.max(...primeFactors) < Math.sqrt(n)) {
            totalSquareRootSmoothNumbers++;
        }
    
    }
    console.log(totalSquareRootSmoothNumbers)
    console.timeEnd();
}

problem668(100);      // 29      default: 3.105ms
problem668(1000000);  // 268172  default: 306.831ms
problem668(10000000); // 2719288 default: 6.739s
```
This correctly calculates the answer for $n = 100$ as explained in the problem as expected but for $n = 10^7$ it is already taking 6.7 seconds. If we want to know the answer when $n = 10^{10}$, we are going to need to be more efficient and improve on this.

## Small Improvements

One small improvement I noticed I could make to `Problem668 v1` would be to break even earlier. We do not need to reduce the number $n$ all the way down to `n === 1` to calculate every prime factor. Once $n$ has been reduced below $\sqrt{n}$ then it is irrelevant what the remaining prime factors are as they all must be below $\sqrt{n}$.

Example: for $1200$, we have $\sqrt{1200} = 34.641\ldots$

$$
\begin{aligned}
1200 \div 2 &= 600\\\
600 \div 2 &= 300\\\
300 \div 2 &= 150\\\
150 \div 2 &= 75\\\
75 \div 3 &= 25\\\
\end{aligned}
$$

In this case, although we have only calculated up to $1200 = 2\times 2\times 2\times 2\times 3\times 25$, we do not need to reduce the last part down to $5\times 5$ as $25$ is already below $\sqrt{1200}$ which is sufficient to prove $1200$ is a square root smooth number.

Furthermore, in the original solution, we use an `Array` to keep track of all prime factors and then use `Math.max()` to calculate the largest one. It is fairly common knowledge that `Array.push()` is a slow call and it is much more efficient to simply track `largestPrimeFactor` rather than keeping them all. 

Finally, I realised that if $n$ was prime, it would be trying and failing to divide by every prime number until the prime number equals $n$. However, we already know that if we are unable to divide $n$ by a number up to its square root, then $n$ must be prime (and therefore not a square root smooth number) so we can break early if the prime number we are trying to divide by becomes greater than $\sqrt{n}$. With these changes (highlighted in the snippet), we now have:

```js title="calcPrimeFactors.js v2" showLineNumbers
const calcPrimeFactors = n => {
    // highlight-next-line
    let largestPrimeFactor = 0; // keep only largest prime factor
    // highlight-next-line
    const sqrtN = Math.sqrt(n);
    for (const prime of primes) {
        // highlight-next-line
        if (n < sqrtN || prime > sqrtN) break; // break even earlier
        while (n % prime === 0) {
            n /= prime;
            // highlight-next-line
            largestPrimeFactor = prime
        }
    }
    // highlight-start
    if (n > largestPrimeFactor) {
        largestPrimeFactor = n;
    }
    return largestPrimeFactor;
    // highlight-end
}

...

problem668(100);      // 29      default: 3.06ms
problem668(1000000);  // 268172  default: 240.838ms
problem668(10000000); // 2719288 default: 5.586s
```
This is a good improvement to `v1` but too little to make any progress on the problem. If I wanted to solve the problem, I would need a completely new approach. I had a few other ideas here and there but when comparing their times, they all failed to make any noticeable improvements. I felt like I had optimised this algorithm as far as possible and for the next couple of days I hit a wall and made zero progress...

## The Eureka Moment!

Quite often when you hit a wall, you need to try and flip your thinking. My `v2` algorithm was an efficient way to go through every number $n$ and determine whether or not $n$ is a square root smooth number depending on its prime factors. However, with $n$ being such a big range, this algorithm is simply too time consuming. What if we flip it around, and try to consider instead, for each prime factor, what values of $n$ become disqualified as a square root smooth number?

If we decipher the original problem carefully, for a prime factor $p$, we know that if a number $n$, is divisible by $p$ and $p \ge \sqrt{n}$ then it would be disqualified as a square root smooth number. The divisibility of $n$ by $p$ can be mathematically stated as $\frac{n}{p}\to\N$, meaning $n$ divided by $p$ belongs to the set of natural numbers (i.e., $\frac{n}{p}$ is a whole number). So bringing them together, we can mathematically state that for a prime factor $p$, the numbers disqualified as a square root smooth numbers are:

$$
D_p = \frac{n}{p}\to\N \text{ and } p \ge \sqrt{n}
$$
Which can be rewritten as:
$$
D_p = \{pn\in \N \mid n \in \N, n \le p \}
$$

This states that the set of disqualified numbers for a given prime factor, $D_p$, are the numbers that are a multiple of $p$, and the number multiplying $p$ are whole numbers from $1$ up to and including $p$. As most of the readers here would be developers, this is what the above set translates to in code:

```js
const Dp = p => {
    const setOfDisqualifiedNumbers = [];
    for (let n = 1; n <= p; n++) {
        setOfDisqualifiedNumbers.push(p * n);
    }
    return setOfDisqualifiedNumbers;
}

console.log('Dp(5)', Dp(5)) // output: Dp(5) [ 5, 10, 15, 20, 25 ]
```

Or more succinctly:

```js live title=D_p.js
log(() => {
    const Dp = p => Array.from(Array(p)).map((_, i) => (i + 1) * p);

    const p = 5;
    return `Dp(${p}) = { ${Dp(p).join(', ')} }`;
})
```

This is best seen by example. Below is a table representing the solution where the limit is $50$:

For $1\ge n\ge 50$:

$p$ (prime factor) |$p^2$|$D_p$ (Set of numbers disqualified) |Count
---|---|---|---
$2$|$4$|$\{2, 4\}$|$2$
$3$|$9$|$\{3, 6, 9\}$|$3$
$5$|$25$|$\{5, 10, 15, 20, 25\}$|$5$
$7$|$25$|$\{7, 14, 21, 28, 35, 42, 49\}$|$7$
$11$|$25$|$\{11, 22, 33, 44\}$|$4$
$13$|$25$|$\{13, 26, 39\}$|$3$
$17$|$25$|$\{17, 34\}$|$2$
$19$|$25$|$\{19, 38\}$|$2$
$23$|$25$|$\{23, 46\}$|$2$
$29$|$25$|$\{29\}$|$1$
$31$|$25$|$\{31\}$|$1$
$37$|$25$|$\{37\}$|$1$
$41$|$25$|$\{41\}$|$1$
$43$|$25$|$\{43\}$|$1$
$47$|$25$|$\{47\}$|$1$

Astonishingly, we can clearly see that the first four prime factors disqualify the same count of $n$ as its own value! Equally important, we can see that none of the disqualified numbers overlap either - this is due to the nature of prime numbers. The fact that the count of disqualified numbers match the value of $p$ is actually obvious if we break it down. For a prime factor $p$, it will disqualify all values of $n$ that are less than $p^2$ and are divisible by $p$, which can be listed as:

$$
\begin{aligned}
&1\times p\\\
&2\times p\\\
&3\times p\\\
&\cdots \\\
&p\times p
\end{aligned}
$$

These are clearly all the numbers divisible by $p$ and less than or equal to $p^2$, and therefore all the numbers disqualified by $p$. So given any prime factor, $p$, they will disqualify exactly $p$ numbers. But there is a slight catch, as seen in the table, primes past $p = 7$ are disqualifying less numbers than $p$, and this is because some of the numbers they disqualify are greater than the max limit of $50$. This means, for a given limit, $l$, if $p^2\le l$, we count all numbers disqualified as $p$, but if $p^2\gt l$, then we only disqualify numbers that fit within the limit. That is, when $p^2\gt l$, the total numbers disqualified is equal to: $\lfloor{\frac{l}{p}\rfloor}$. This is equivalent to the following code:

```js showLineNumbers
let disqualifiedNumbers = 0;

if (p ** 2 <= limit) {
    disqualifiedNumbers += p;
} else {
    disqualifiedNumbers += Math.floor(limit/p);
}
```

Using this logic, we can easily total up the number of disqualified numbers and subtract to find the number of remaining numbers which would be square root smooth. That is:

$$
\sum{\text{square root smooth}} = \text{limit} - \sum{D_p}
$$

Looking at the table earlier for $1\ge n\ge 50$, we can see the total number of disqualified numbers are:

$$
2 + 3 + 5 + 7 + 4 + \cdots + 1 + 1 + 1 = 36
$$

Therefore, the total number of square root smooth numbers are $50 - 36 = 14$. The full code for `v3` is now:

```js title="Problem668 v3" showLineNumbers
const problem668_v3 = limit => {
    console.time();
    const sqrtLimit = Math.sqrt(limit);
    const primes = genPrimes(limit);
    let nonSmooth = 0;

    for (const p of primes) {
        if (p <= sqrtLimit) {
            nonSmooth += p;
        } else {
            nonSmooth += Math.floor(limit/p);
        }
    }
    
    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers);
    console.timeEnd();
}

problem668_v3(50);       // output: 14      default: 3.427ms
problem668_v3(100);      // output: 29      default: 0.045ms
problem668_v3(1000000);  // output: 268172  default: 19.859ms
problem668_v3(10000000); // output: 2719288 default: 99.317ms
```

We can see that this new algorithm still produces the same results as the algorithm in `v2` but runs substantially faster.

Version | Limit | Duration (ms)| % Faster than v1
---|---|---|---
v1 | $100$ | 3.105 | &ndash;
v1 | $10^6$ | 306.831 | &ndash;
v1 | $10^7$ | 6739 | &ndash;
v2 | $100$ | 3.06 | 1.47%
v2 | $10^6$ | 240.838 | 27.4%
v2 | $10^7$ | 5586 | 20.6%
v3 | $100$ | 0.045 | 6,800%
v3 | $10^6$ | 19.859 | 1,445%
v3 | $10^7$ | 99.317 | 6,685%

With this new algorithm being so much faster, we can now solve the problem by running our code with `n = 10 ** 10`, right?

```
> node ProjectEuler/problem668.js

/coding_challenges/JSUtils/genPrimes.js:13
    let sieve = Array(arrSize).fill(0);
                ^

RangeError: Invalid array length
    at genPrimes (/coding_challenges/JSUtils/genPrimes.js:13:17)
    at problem668_v3 (/coding_challenges/ProjectEuler/problem668.js:39:20)
    at Object.<anonymous> (/coding_challenges/ProjectEuler/problem668.js:62:1)
    at Module._compile (node:internal/modules/cjs/loader:1256:14)
    at Module._extensions..js (node:internal/modules/cjs/loader:1310:10)
    at Module.load (node:internal/modules/cjs/loader:1119:32)
    at Module._load (node:internal/modules/cjs/loader:960:12)
    at Function.executeUserEntryPoint [as runMain] (node:internal/modules/run_main:81:12)
    at node:internal/main/run_main_module:23:47
```

Wrong! We have an error with array length in our `genPrimes` function... what if we try `n = 10 ** 9` instead?

```
> node ProjectEuler/problem668.js


<--- Last few GCs --->

[67457:0x118040000]     1765 ms: Scavenge 517.2 (551.8) -> 517.2 (551.8) MB, 105.0 / 0.0 ms  (average mu = 1.000, current mu = 1.000) allocation failure; 
[67457:0x118040000]     2435 ms: Mark-sweep 901.2 (935.8) -> 900.6 (935.8) MB, 412.7 / 0.0 ms  (+ 0.2 ms in 225 steps since start of marking, biggest step 0.0 ms, walltime since start of marking 1409 ms) (average mu = 1.000, current mu = 1.000) allocation

<--- JS stacktrace --->

FATAL ERROR: invalid table size Allocation failed - JavaScript heap out of memory
```

It seems we have ran out of memory... so what's going on here? The problem is, although our algorithm for `v3` is much faster than previous versions, the previous algorithms only required prime numbers up to $\sqrt{\text{limit}}$. However, in `v3` we need the prime numbers all the way up to the limit. So although we have sped up the algorithm by a considerable amount, now the problem switches to how to calculate every prime number up to the limit. And with the current implementation of `genPrimes` that I had, it simply was not efficient enough to calculate so many primes.

## Calculating Primes

I have been deliberately avoiding the `genPrimes` function until now because this becomes the main part of the problem. Based on our previous knowledge, we know that we can calculate if a number is prime by checking if it is divisible by a number, $d$, where $2 \le d \le \sqrt{p}$.

```js title="genPrimesV1.js" showLineNumbers live
log(() => {
    const genPrimesV1 = limit => {
        // console.time();
        const primes = [];
        for (let n = 2; n <= limit; n++) {
            let isPrime = true;
            for (let d = 2; d <= Math.sqrt(n); d++) {
                if (n % d === 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) primes.push(n);
        }
        // console.timeEnd();
        return primes;
    }

    return genPrimesV1(100).join(', ')
    // genPrimesV1(1000)    // default: 0.238ms
    // genPrimesV1(10 ** 6) // default: 82.381ms
    // genPrimesV1(10 ** 7) // default: 1.964s
    // genPrimesV1(10 ** 8) // default: 51.945s
})
```

This is a naive way of calculating primes and is too slow if we want primes up to $10^{10}$. There is a faster method known as [sieve of Eratosthenes](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes) which works by starting with all of the numbers from $2$ to the limit you want and iteratively cancel out non-primes. The first non-cancelled number is a prime, which starts with $2$. You then start to cancel out all multiples of $2$ starting from $2^2$ and then you move on to the next non-cancelled number (which will be $3$, and therefore $3$ is a prime) and continue until you reach the square root of the limit. When you have finished the process, you will have found all prime numbers from $2$ to the limit as they are all the numbers that were not cancelled out.

import SieveAnimation from '@site/static/img/animated_sieve.gif';

<div style={{ width: '100%', display: 'flex' }}>
<figure style={{ margin: '0 auto 1rem auto'}}>
  <img src={SieveAnimation} style={{ width: 'auto' }} />
<figcaption>Fig.1 - Animated sieve of Eratosthenes for numbers below 121.</figcaption>
</figure>
</div>

We can code the sieve by initializing an `Array` from $2$ to the limit and looping through to cancel out the non-primes as described before:

```js title="genPrimesV2.js" showLineNumbers live
log(() => {
    const genPrimesV2 = limit => {
        // console.time();
        const sieve = Array.from(Array(limit - 1)).map((_, i) => i + 2);
        for (const n of sieve) {
            if (!n) continue; 
            for (
                let cancel = (n ** 2) - 2;
                cancel < sieve.length;
                cancel += n
            ) {
                sieve[cancel] = null;
            }
        }

        const primes = sieve.filter(Boolean);
        // console.timeEnd();
        return primes;
    }

    return genPrimesV2(100).join(', ');
    // genPrimesV2(1000)    // default: 0.145ms
    // genPrimesV2(10 ** 6) // default: 40.411ms
    // genPrimesV2(10 ** 7) // default: 334.173ms
    // genPrimesV2(10 ** 8) // default: 7.266s
})
```

This is much faster than `genPrimesV1`, unfortunately when we try with a limit of $10^9$, we start running out of memory. In `v1`, we start with an empty array of primes and add to it as we find primes. However, in `v2`, we initialize with an `Array` of length `limit - 1` which causes memory issues.

## Optimising the Sieve Method

We need to understand how `Array` works to optimise this algorithm. The current method requires storing large numbers in the array which takes up memory, but we can actually be smart about it and make use of the index. If you think about the sieve, we only truly need a `Boolean` for each item to tell us whether the number has been _"cancelled"_ or not. And we can make use of the index as the values of the sieve, but we must add two to the index as the sieve should start from `2` but obviously the index starts from `0`.

```js title="genPrimesV3.js" showLineNumbers
const genPrimesV3 = limit => {
    console.time();
    const sieve = Array(limit - 1).fill(true);
    for (let index = 0; index < sieve.length; index++) {
        const sieveValue = sieve[index];
        const prime = index + 2;
        if (!sieveValue) continue;
        for (
            let cancel = (prime ** 2) - 2;
            cancel < sieve.length;
            cancel += prime
        ) {
            sieve[cancel] = false;
        }
    }
    
    const primes = sieve.reduce((primes, val, i) => {
        if (val) {
            primes.push(i + 2);
        }
        return primes;
    }, [])
    console.timeEnd();
    return primes;
}

genPrimesV3(1000)    // default: 0.122ms
genPrimesV3(10 ** 6) // default: 13.438ms
genPrimesV3(10 ** 7) // default: 122.578ms
genPrimesV3(10 ** 8) // default: 4.382s
```

Not only does this save memory, but it even speeds it up! However, we will still run into memory issues with the above code even though we are only storing a boolean in the array. This is where Javascript's TypedArrays, specifically `Uint8Array` comes in. This is the array that has the lowest number of bits assigned to each element, specifically 8 bits, or one byte. This means that we can only hold values from 0 - 255 at most but we only require a 1 or a 0 so it's fit for our purpose. Furthermore, with the normal `Array`, we needed to filled it with `true` at the start, which takes a fair chunk of time. A `Uint8Array` initializes with all `0`s so we can take this to our advantage. We can flip the boolean check and assume that if the value of the sieve is `falsey`, then it is a prime, and we _"cancel"_ the number by making it truthy, such as setting it to `1`. By doing this inversion, we save ourselves from having to `.fill()` which produces the following code:

```js title="genPrimesV4.js" showLineNumbers
const genPrimesV4 = limit => {
    console.time();
    const sieve = new Uint8Array(limit - 1);
    for (let index = 0; index < sieve.length; index++) {
        const sieveValue = sieve[index];
        const prime = index + 2;
        if (sieveValue) continue;
        for (
            let cancel = (prime ** 2) - 2;
            cancel < sieve.length;
            cancel += prime
        ) {
            sieve[cancel] = 1;
        }
    }
    
    const primes = sieve.reduce((primes, val, i) => {
        if (!val) {
            primes.push(i + 2);
        }
        return primes;
    }, [])
    console.timeEnd();
    return primes;
}

genPrimesV4(1000)    // default: 0.141ms
genPrimesV4(10 ** 6) // default: 14.52ms
genPrimesV4(10 ** 7) // default: 116.937ms
genPrimesV4(10 ** 8) // default: 1.434s
genPrimesV4(10 ** 9) // default: 15.190s
```

We are now able to calculate primes up to $10^9$ in under 16 seconds! Unfortunately, even with `Uint8Array` we are still unable to calculate for a limit of $10^{10}$...

```
> node ProjectEuler/problem668.js

/coding_challenges/ProjectEuler/problem668.js:154
    const sieve = new Uint8Array(limit - 1);
                  ^

RangeError: Invalid typed array length: 9999999999
    at new Uint8Array (<anonymous>)
```

## The Last Piece of the Puzzle?

The remaining options to solving this problem is pretty limited. We have a decent algorithm and just require an Array that is able to hold enough elements to calculate the primes. So the next step is naturally to create our own `Uint1Array`. The `Uint8Array` was good but as we still require more memory, we can build a custom `Uint1Array` that will use one bit of memory for each element. This can be achieved by using the `Uint8Array` but splitting each element into eight more sub elements. The mapping of the index from `Uint1Array` to the `Uint8Array` can be calculated simply as:

```
const uint8ArrayIndex = Math.floor(index / 8);
const bitIndex = index % 8;
```

For a given index, however many eights can fit into the index will be the index of the `Uint8Array` and the remainder will be the index of the sub element within the main element.

E.g., index of 50: when divided by 8 is equal to 6 remainder 2, therefore the 6th element of the `Uint8Array` and the 2nd sub element.

Storing of sub elements can be done using some binary knowledge and bit operators. We know that the number 255 can be written in binary as `11111111`. This can be interpreted as eight `true`. So if we wanted the 2nd sub element to be true, and everything else false, the binary should be `00000010` (it gets flipped in binary from left to right), which is the number 2. We can manipulate the binary by adding or subtracting powers of $2$, and use bit masking to read the values.

Example - we want to know if the 2nd sub element is true when the number in the main element is 171:

$$
\begin{aligned}
 \begin{split}
&101010\textbf{1}1 = 171\\
\text{AND}\ &000000\textbf{1}0 = 2^1\\
    \hline
    &000000\textbf{1}0
  \end{split}
\end{aligned}
$$

The AND operator simply takes each bit and checks if both are $1$ - in the example, $2^1$ only has a single $1$ in the second 'index' so every other bit will become zero. Then the result will only be truthy if the number $171$ also contains a $1$ in the same second 'index' slot. This allows you to read whether the value of the sub element is true or false, simply by performing AND with the number $2^{i}$, where $i$ is the sub index. Putting this altogether, we can create a class `Uint1Array` as follows:

```js title="Uint1Array.js" showLineNumbers
/**
 * Makes use of Uint8Array to create a Uint1Array with two methods:
 * set(index, value): sets true or false at given index
 * get(index): returns true or false at given index
 * @param {range to end at, including this} n
 */
class Uint1Array {
    constructor(length) {
        this.uint8Array = new Uint8Array(Math.ceil(length/8) + 1);
        this.length = this.uint8Array.length * 8;
    }

    set(index, value) {
        const uint8ArrayIndex = Math.floor(index / 8);
        const bitIndex = index % 8;
        const currentBitValue =
            !!(this.uint8Array[uint8ArrayIndex] & (2 ** bitIndex))

        // want it true but currently false
        if (value && !currentBitValue) {
            this.uint8Array[uint8ArrayIndex] += 2 ** bitIndex;
        // want it false but currently true
        } else if (!value && currentBitValue) {
            this.uint8Array[uint8ArrayIndex] -= 2 ** bitIndex;
        }
    }

    get(index) {
        const uint8ArrayIndex = Math.floor(index / 8);
        const bitIndex = index % 8;
        return !!(this.uint8Array[uint8ArrayIndex] & (2 ** bitIndex))
    }
}

module.exports = Uint1Array;
```

Although this works, sadly the `Uint1Array` makes it run considerably slower. This is because it is not optimised and the `.reduce()` function used for gathering the primes from the sieve is also not implemented which means more time again to loop through our sieve.

```js title="genPrimesV5.js" showLineNumbers
const genPrimesV5 = limit => {
    console.time();
    const sieve = new Uint1Array(limit - 1);
    for (let index = 0; index < limit - 1; index++) {
        const sieveValue = sieve.get(index);
        const prime = index + 2;
        if (sieveValue) continue;
        for (
            let cancel = (prime ** 2) - 2;
            cancel < limit - 1;
            cancel += prime
        ) {
            sieve.set(cancel, 1);
        }
    }

    const primes = [];
    for (let i = 0; i < limit - 1; i++) {
        const sieveValue = sieve.get(i);
        if (!sieveValue) {
            primes.push(i + 2);
        }
    }
    console.timeEnd('end');
    return primes;
}

genPrimesV5(1000)    // default: 1.013ms
genPrimesV5(10 ** 6) // default: 190.711ms
genPrimesV5(10 ** 7) // default: 1.991s
genPrimesV5(10 ** 8) // default: 21.724s
genPrimesV5(10 ** 9) // default: 3:58.933 (m:ss.mmm)
```

Comparison of all `genPrimes` so far:

Version | Limit | Duration (seconds) | % Faster than v1
---|---|---|---
v1 | $10^8$ | 51.945 | &ndash;
v2 | $10^8$ | 7.266 | 614.9%
v3 | $10^8$ | 4.382 | 1085.4%
v4 | $10^8$ | 1.434 | 3522.4%
v5 | $10^8$ | 21.724 | 139.1%

If we want to solve for limit = $10^{10}$, this seems like it could take hours, or potentially days when using `v5`. However, we can enhance the performance of `v5` by removing the flaw of the missing `.reduce()` by removing the need for pushing primes if we combine our code with the sieve. This is possible since the sieve tells us what the primes are on-the-go. Furthermore, although it adds a bit of complexity, we can set up the sieve to only check for odd numbers to essentially cut the sieve in half as we can jump in steps of 2 by mapping the `index` to `index * 2 + 3`. Putting everything together we get:

```js title="problem668_v4.js" showLineNumbers live
log(() => {
    const problem668_v4 = limit => {
        // console.time();
        const sqrt = Math.sqrt(limit);
        let nonSmooth = 2; // start with 2 to skip all even number checks
        const arrSize = Math.ceil(limit / 2) - 1;
        let sieve = new Uint1Array(limit)
        for (let i = 0; i < arrSize; i++) {
            // mark all odd numbers from 3 as potential primes
            sieve.set(i * 2 + 3, true);
        }

        for (let i = 0; i < arrSize; i++) {
            const sieveValue = sieve.get(i * 2 + 3);
            if (!sieveValue) continue;
            const prime = i * 2 + 3;
            nonSmooth += prime <= sqrt ? prime : Math.floor(limit/prime);
            const idxSquare = i + (prime * (prime - 1)) / 2;
            for (let j = idxSquare; j < arrSize; j += prime) {
                // marking every nth as not prime starting from p^2
                sieve.set(j * 2 + 3, false);
            }
        }

        const smoothNumbers = limit - nonSmooth;
        // console.timeEnd();
        return `For limit = ${limit}, there are ${smoothNumbers} square root smooth numbers.`;
    }

    // WARNING: I do not recommend running above 10 ** 7 on browser
    return problem668_v4(10 ** 2);

    // WARNING: DO NOT RUN THIS ON THE BROWSER AS IT WILL CRASH IT
    // problem668_v4(10 ** 10); // default: 37:26.146 (m:ss.mmm)
})
```

We have at last solved the problem in under 40 minutes! It was a long journey from having a bad algorithm to the Eureka! moment, only to have a completely new problem pop up. In the end, it was all a learning experience and an enjoyable journey. It taught me to try think outside the box more and try to flip the problem around. I thoroughly enjoyed coding the sieve of Eratosthenes too! The `Array` running out of memory was new to me as I had never encountered a problem that required so much elements until now, which led to my discovery of Javascript's `TypedArray` classes.

...However, if you take a look at Project Euler's home page, it states:

<ProjectEuler>

**I've written my program but should it take days to get to the answer?**

Absolutely not! Each problem has been designed according to a "one-minute rule", which means that although it may take several hours to design a successful algorithm with more difficult problems, an efficient implementation will allow a solution to be obtained on a modestly powered computer in less than one minute.

</ProjectEuler>

Curious, I wanted to test other languages as we all know Javascript is not really built for this. I have been interested in a language called [Julia](https://julialang.org/) as I see it being used with great success on Project Euler. I implemented my algorithm in Julia and the results were astonishing.

```julia title=calcSquareRootSmooth.jl showLineNumbers
using FastPrimeSieve, Printf

calcSquareRootSmooth = function(limit)
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(limit)));

    nonSmooth = 0;

    for prime in primes
        if prime <= sqrt(limit)
            nonSmooth += prime
        else
            nonSmooth += floor(limit/prime)
        end
    end
    @printf "For limit %i, there are %i square root smooth numbers." limit (limit - nonSmooth)
end

@time calcSquareRootSmooth(10^10)

# For limit 10000000000, there are XXXXXXXXXX square root smooth numbers.
# 14.120286 seconds (12.94 k allocations: 7.092 GiB, 0.10% gc time, 0.08% compilation time)
```

It was able to calculate the correct answer for $n = 10^{10}$ in under 15 seconds!

import { WaiSignature } from '../src/components/WaiSignature';

<WaiSignature />
