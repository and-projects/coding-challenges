---
slug: hybrid-integers
title: Hybrid Integers
authors: [wai]
tags: [project-euler, optimisation]
date: 2023-09-19
---

I have been working through many project euler problems and realised I was close to finishing all of the 5% difficulty problems so I have been targeting these problems recently. This is my journey on solving [Problem 800, Hybrid Integers](https://projecteuler.net/problem=800) using Julia:

import { ProjectEuler } from '../src/components/ProjectEuler';

<ProjectEuler>

**Problem 800**

An integer of the form $p^qq^p$ with prime numbers $p \ne q$ is called a <em>hybrid-integer</em>.
For example, $800 = 2^55^2$ is a hybrid-integer.

We define $C(n)$ to be the number of hybrid-integers less than or equal to $n$.
You are given $C(800) = 2$ and $C(800^{800}) = 10790$.

Find $C(800800^{800800})$.
</ProjectEuler>

<!-- truncate-->

:::caution

Everything from here on are huge spoilers to the problem. If you want to attempt the problem yourself first, look away now. You have been warned!
:::

## Initial Thoughts

The most logical solution to the problem would be to avoid calculating the huge powers, and we can do this using [logarithm](https://en.wikipedia.org/wiki/Logarithm) from mathematics. First, to simplify the problem and avoid calculating $800800^{800800}$, we can consider a modified function, $C'$ where the input is just the base to avoid calculating big powers. So the modified function is equivalent to the following:

$$
\begin{aligned}
C'(800) &= C(800^{800})\\\
C'(800800) &= C(800800^{800800})
\end{aligned}
$$

Then, my idea would be to convert $p^q$ and $q^p$ to the form $800800^x$, then we can simply compare the powers and see if our number is bigger or smaller than $n$. The rules of logarithm we will be using are:

$$
\begin{equation}
\log(a^b) = b \log a
\end{equation}
$$

$$
\begin{equation}
\log(ab) = \log a + \log b
\end{equation}
$$

We will also make use of the rule for combining powers:

$$
\begin{equation}
a^ba^c = a^{b+c}
\end{equation}
$$

Using a combination of these rules, we can change the base of any number being raised to a power:
$$
\begin{aligned}
a^x &= p^q\\\
\log(a^x) &= \log(p^q)\\\
x \log a &= q\log p\\\
x &= \frac{q\log p}{\log{a}}
\end{aligned}
$$

### Example

we want to check if $23^{47}47^{23}$ is less than $80^{80}$ or not:

1. convert $23^{47}$ to $80^x$:

$$
x = \frac{47\log{23}}{\log{80}} = 33.63\dots
$$

2. convert $47^{23}$ to $80^y$:

$$
y = \frac{23\log{47}}{\log{80}} = 20.21\dots
$$

Therefore: $23^{47}47^{23} \approx 80^{33.63}80^{20.21}$, and using rule $(3)$, we get:

$$
80^{33.63}80^{20.21} = 80^{33.63+20.21} = 80^{53.84}
$$

We can clearly see that $80^{53.84} \lt 80^{80}$ just by comparing their powers without calculating what the actual number is after being raised to the power, and therefore, we now know that $23^{47}47^{23}$ is less than $80^{80}$ with certainty.

### Calculating the Upper Boundary

The problem states that $p$ and $q$ are different primes and seeing $C(800) = 2$ suggests that we only need to calculate when $q > p$, because they only count $2^55^2$ as a solution, but not the reverse $5^22^5$ (the two solutions for $C(800)$ are $2^33^2$ and $2^55^2$).

We can calculate an upper bound, $q_{max}$ by noticing that as the numbers $p$ and $q$ get large, $p^q$ gets considerably larger than $q^p$. So for the upper boundary, we can simply ignore the second half and only consider when $p = 2$, which is when $q$ will be at its highest:

$$
\begin{aligned}
2^{q_{max}} &= n^n\\\
q_{max} &= \Bigl\lceil\frac{n\log n}{\log 2}\Bigr\rceil
\end{aligned}
$$

So for example, when we have $C'(800)$, or $C(800^{800})$, we get an upper boundary of:

$$
q_{max} = \Bigl\lceil\frac{800\log 800}{\log 2}\Bigr\rceil = \lceil7715.08\dots\rceil = 7716
$$

Therefore, when calculating $C'(800)$, we only need primes up to and including $7716$ at most to capture the full range. We are overestimating the range as we only consider $2^q$ part and ignoring $q^2$ but the estimation is ***extremely good*** and requires little computation. For comparison, the first prime number that goes beyond the limit is $7687$:

$$
2^{7687}7687^2 \gt 800^{800}
$$

which is only 29 lower than the estimated upper bound! By using the estimated upper bound of $7716$, we would only have to calculate an extra three primes which will not make any real difference in the grand scheme of things.

### First Iteration
Putting everything together for the first iteration, to calculate $C'(n)$, we need primes from $2$ to $q_{max}$, and for each $p$, where $2 \le p \le q_{max}$, we need to find how many values of $q$ there are such that $p^qq^p \le n^n$ where $p < q \le q_{max}$:

```julia title=problem800.jl showLineNumbers
using Primes

function convertBase(base, power, newBase)
    return power * log(base) / log(newBase);
end

function C′(n)
    primeLimit::Int64 = ceil(n*log(n)/log(2))
    prime_numbers = primes(primeLimit)
    total = 0;

    for (p_index, p) ∈ enumerate(prime_numbers)
        # no more sols when p ^ first q > n as numbers will only grow
        if convertBase(p, prime_numbers[p_index + 1], n) > n
            break
        end

        for q ∈ prime_numbers[p_index + 1: end]
            adjusted_p_power = convertBase(p, q, n)
            adjusted_q_power = convertBase(q, p, n)
            if adjusted_p_power + adjusted_q_power > n
                break;
            end
            total += 1
        end
    end

    return total;
end


@time @show C′(800);
@time @show C′(800800);

# C′(800) = 10790
# 0.000401 seconds (156 allocations: 1011.055 KiB)
# C′(800800) = **********
# 63.597286 seconds (128.06 k allocations: 468.083 GiB, 10.72% gc time)
```

This initial version is already able to solve the problem! However, it takes over one minute to solve it. Can we improve on this?

## Small Improvements

The first improvement we can make is by applying the laws of logs in a better way. Converting the base requires two log calculations each time but we can reduce this by converting the original problem:

$$
\begin{aligned}
p^qq^p &= n^n\\\
q \log p + p \log q &= n \log n
\end{aligned}
$$

By comparing the values this way, and switching to a sieve for calculating primes (see previous [blog](./square-root-smooth-numbers)) we save almost 50% of execution time:

```julia title=problem800v2.jl showLineNumbers
using FastPrimeSieve

function C′(n)
    pLimit::Int64 = ceil(n*log(n)/log(2))
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pLimit)));
    total = 0;

    log_limit = n * log(n);

    for (p_index, p) ∈ enumerate(primes)
        log_p = log(p);
        smallest_q = primes[p_index + 1];

        # no more sols when p ^ first q > n as numbers will only grow
        if p * log(smallest_q) + smallest_q * log_p > log_limit
            break
        end

        for q ∈ primes[p_index + 1: end]
            if p * log(q) + q * log_p > log_limit
                break;
            end
            total += 1
        end
    end

    return total;
end

# C′(800800) = **********
# 34.294069 seconds (70.85 k allocations: 262.705 GiB, 13.03% gc time)
```

It would actually be great if we could find the first value of $q$ that breaks past the limit and we can just use the index to count how many $q$'s there were to add to the total, such as:

```julia
q_index = findfirst(
    q -> p*log(q) + q*log_p > log_limit,
    primes[p_index+1:length(primes)]
)
total += q_index - 1
```

However, using this trick doesn't really save us any time because the function `findfirst` still needs to loop through all the primes until it finds the first $q$ that breaks the limit. If only there was a better way...

## Binary Search

Whenever we are searching for a value in a *sorted* list, we should always consider a [binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm). The way binary search works is simple:

1. Find the midpoint of the sorted list
2. Compare the value of the element in the middle with the search criteria:
    1. If the value we want is higher than the middle element, we know it must live in the second half of the list
    2. If the value we want is lower than the middle element, we know it must live in the first half of the list instead
4. go back to step 1. with half of the list until the middle element equals the criteria

### Example

List = `[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]`, Desired number = `13`:

1. Middle number = 11
2. 13 > 11, therefore repeat with second half of the list
3. List is now `[12,13,14,15,16,17,18,19,20]`
4. Middle number = 16
5. 13 < 16, therefore repeat with first half of the list
6. List is now `[12,13,14,15]`
7. Middle number = 14
8. 13 < 14, therefore repeat with the first half of the list
9. List is now `[12,13]`
10. Middle number = 13, we have found our number

From the example, I have used the middle number as the rounded up index when the list is an even number, but either one would work. From the example, we only had to make 4 comparisons to get to the number 13, where as if we simply looped through the list, it would have taken 13 comparisons.

### Applying the Binary Search to our Problem

In our problem, we are trying to search for the first $q$ such that: $p \log q + q \log p > n \log n$ from a sorted list of prime numbers in ascending order. We need a comparison function that tells the binary search if our desired number is higher or lower than the current middle element. When the value of $q$ gives us a value lower than $n \log n$, we know that $q$ must be bigger (i.e. in the second half of the list of primes). Conversely, when we find a value of $q$ such that the value is greater than $n \log n$, we know it *could* be the answer, but we still need to check for any earlier values of $q$ which may still satisfy the condition as we want specifically the **first** $q$ that breaks the limit.

Here is my attempt at writing a generic binary search function that will return the index of the first element that satisfies a condition:

```julia title=binarysearchfindfirst.jl showLineNumbers
function binarysearchfirst(list, compare, found = nothing, index = 0)
    if length(list) < 3
        ans = findfirst(n -> compare(n) === 0, list)
        return ans === nothing ? found : index + ans;
    end

    mid = cld(length(list), 2)

    firstHalf = @view list[1:mid-1]
    secondHalf = @view list[mid + 1:end]

    ans = compare(list[mid])
    if ans === 0
        return binarysearchfirst(firstHalf, compare, index + mid, index)
    elseif ans === 1
        return binarysearchfirst(firstHalf, compare, found, index)
    else
        return binarysearchfirst(secondHalf, compare, found, index + mid)
    end
end
```

The parameter `condition` is a function the binary search function calls to compare the current value with the desired value and depending on whether it returns a `0`, `1`, or `-1`, it will know to look earlier or further ahead. The parameter `found` is used to keep track of the current earliest possible answer (we cannot confirm it's the first one until we finish the algorithm). The parameter `index` is used to keep track of the index with respect to the original list as we move deeper into sublists. One of the most interesting part of the binary search is the use of Julia's `@view` macro. I could have just set the halves of the lists as normal:

```julia
firstHalf = sortedlist[1:mid-1]
secondHalf = sortedlist[mid + 1:end]
```

However, doing so, the above code makes a new *copy* of the list for the first and second half. This creation of new copies of the array is a very slow process. Julia's `@view` macro allows you to create sublists *without* making a new copy. It gives you a sliced `view' of the original list that acts exactly like a sublist. Since this does not make new copies of the array, this saves a tremendous amount of time. In essense, it is passing value by reference rather than creating new arrays. For more information, see [Julia - Arrays/Views Documentation](https://docs.julialang.org/en/v1/base/arrays/#Views-(SubArrays-and-other-view-types)).


By implementing the binary search into our algorithm, along with the cheeky `@view` macro, the final algorithm is:

```julia title=problem800v3.jl showLineNumbers
function C′(n)
    pLimit::Int64 = ceil(n*log(n)/log(2))
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pLimit)));
    total = 0;

    log_limit = n * log(n);

    for (p_index, p) ∈ enumerate(primes)
        log_p = log(p);
        smallest_q = primes[p_index + 1];

        # no more sols when p ^ first q > n as numbers will only grow
        if p * log(smallest_q) + smallest_q * log_p > log_limit
            break
        end

        q_primes = @view primes[p_index + 1:end];
        q_index = binarysearchfirst(
            q_primes,
            q -> p * log(q) + q * log_p > log_limit ? 0 : -1
        )
        total += q_index - 1
    end

    return total;
end

# C′(800800) = **********
# 0.019032 seconds (21 allocations: 15.961 MiB)
```

This final solution takes just under 2ms to execute, an improvement of over 317,850% compared to the first iteration which took over one minute!

import { WaiSignature } from '../src/components/WaiSignature';

<WaiSignature />

